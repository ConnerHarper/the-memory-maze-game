﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 
using UnityEngine.UI; 

public class PlayerRaycasting : MonoBehaviour
{
    // Declare global and local variables - local variables are editable in Unity (can add GameObjects to them etc. or called to another script through GetComponent.)
    // Global variables are only accesable by this script
    public float distanceToSee;
    RaycastHit whatIHit;

    public GameObject prompt;
    // State toggle will allow us to switch between 2 states of answering a question and moving around the game world
    // False = move around the world, true = answer a question
    public bool stateToggle = false;
    public ButtonTrigger lastButton;
    CountdownTimer countdown;
    GameObject CountdownObject;
    bool countdownTimer; 

    // Start is called before the first frame update
    void Start()
    {
        // We will find the timer so that we are able to change how the timer works in this script
        CountdownObject = GameObject.Find("Timer");
        countdown = CountdownObject.GetComponent<CountdownTimer>(); 


    }

    // Update is called once per frame
    void Update()
    {
        // These if statements will allow us to toggle between two states (answering a question/moving around the world pressing buttons).
        if (stateToggle == false)
        {
            // We will run the ButtonCollisions if the StateToggle has been disabled
            ButtonCollisions();
            
        }
        else
        {
            // The prompt for the player to press the button with the "F" key will be disabled if StateToggle is true
            prompt.SetActive(false);
        }

        Countdown(); 

        Debug.Log(countdown.currentTime);


    }    

    void ButtonCollisions()
    {
        Debug.DrawRay(this.transform.position, this.transform.forward * distanceToSee, Color.magenta);

        if (Physics.Raycast(this.transform.position, this.transform.forward, out whatIHit, distanceToSee))
        {

            // This will allow us to show a prompt if we are within touching distance of the button
            if (whatIHit.collider.gameObject.name == "Button_Red")
            {

                lastButton = whatIHit.collider.gameObject.GetComponent<ButtonTrigger>();

                if (lastButton.BeenPressed() == false)
                {
                    // If the button has been pressed, we will only have the prompt appear on the buttons that the player has not pressed
                    prompt.SetActive(true);

                    if (Input.GetKeyDown("f"))
                    {

                        // When the button has been pressed, we will set stateToggle to true
                        stateToggle = true;

                        // We will also re-enable the cursor so the player can chose on of the quiz options 
                        Cursor.visible = true;

                        lastButton.QuestionTrigger();

                        // We will enable the countdown timer so that it starts counting down when the player has clicked the button
                        countdownTimer = true;

                        
                        countdown.currentTime = 20f;


                    }
                }

            }

        }
        else
        {
            // We will set the prompt to false if the player is not within touching distance of the button
            prompt.SetActive(false);

        }
    }

    void Countdown()
    {
        

        // We will run the Timer function on the CountdownTimer script if the button has been pressed and currentTime is not equal 0
        if (countdownTimer == true && countdown.currentTime != 0)
        {
            countdown.Timer();

        }

        // We will set countdownTimer to false if the time limit has reached 0 or if the stateToggle is false
        if (countdown.timesUp == true || stateToggle == false)
        {
            Debug.Log("Timer is false");
            countdownTimer = false;

        }
        

    }


}
