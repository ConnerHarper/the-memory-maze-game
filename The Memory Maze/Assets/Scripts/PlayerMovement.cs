﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    // Declare global and local variables - local variables are editable in Unity (can add GameObjects to them etc. or called to another script through GetComponent.)
    // Global variables are only accesable by this script
    public CharacterController controller;

    public float speed = 12f;
    public float gravity = -9.81f;
    public float jumpHeight = 3f; 

    public Transform groundCheck; 
    public float groundDistance = 0.4f; 
    public LayerMask groundMask; 

    Vector3 velocity;
    bool isGrounded;
    public bool stateToggle;
    GameObject obj; 

    // Start is called before the first frame update
    void Start()
    {
        // We must find the Main Camera GameObject so that we can control the main camera within this script
        obj = GameObject.Find("Main Camera");
        

    }

    // Update is called once per frame
    void Update()
    {

        stateToggle = obj.GetComponent<PlayerRaycasting>().stateToggle;

        // These if statements will allow us to toggle between two states (answering a question/moving around the world pressing buttons).
        if (stateToggle == false)
        {
            // If stateToggle is false, then we want to allow the player to move around using WASD keys
            Movement();
        }
        else
        {
           // we also want to do nothing if stateToggle is true
        }

        
    }

    public void Movement()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        // We will check that the player is grounded and velocity is 0, as a check 
        // we will then set at what rate the player is moving in position which we will control the movement with the velocity 
        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * speed * Time.deltaTime);

        // If the player presses the button set to the jump button (Space bar) then we will move the player upwards for a time then back down again, with velocity and gravity
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);

    }
}
