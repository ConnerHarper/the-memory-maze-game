﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    // Declare global and local variables - local variables are editable in Unity (can add GameObjects to them etc. or called to another script through GetComponent.)
    // Global variables are only accesable by this script
    public static bool GameIsPaused = false;
    public GameObject pauseMenuUI; 

    // Update is called once per frame
    void Update()
    {
        // If the player inputs the Escape key, we want to open the pause menu
        if (Input.GetKeyDown(KeyCode.P))
        {
            
            // Tell the game what function to run when the game is paused (resume) and when the game is running (pause)
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    void Pause()
    {
        // If the pauseMenu is active, we want to set the game to freeze, by setting the timeScale to 0 so the game is actually paused
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void Resume()
    {
        // If the player clicks resume on the pause menu - we want to set the timeScale to 1 and unfreeze the game 
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        Cursor.visible = true;
        GameIsPaused = false;
    }


    public void LoadMenu()
    {
        // We want to load the main menu if the player clicks the button that says "Main Menu" and unfreeze the timeScale to 1
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu");
    }

    // If the player clicks the Quit button, we want the game to quit
    public void QuitGame()
    {
        Application.Quit();
    }
}
