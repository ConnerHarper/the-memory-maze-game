﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstrustionsStart : MonoBehaviour
{
    // Declare global and local variables - local variables are editable in Unity (can add GameObjects to them etc. or called to another script through GetComponent.)
    // Global variables are only accessable by this script
    public GameObject remove;
    GameObject instructions;

    void Update()
    {
        // we will find the Instructions Start GameObject so we can enable and disable it when we like
        instructions = GameObject.Find("InstructionsStart");

        // We will set timeScale to 0 so that it freezes the game whilst we are viewing the instructions at the start
        // The timeScale would only be used if it's at the start of the game rather than in the middle of the game itself (e.g. if a timelimit is running) 
        if (instructions == true)
        {
            Time.timeScale = 0f;

            // We will also enable the cursor so the player can click the "ok" button when they have finished reading the instructions
            Cursor.visible = true;
        }
    }

    public void NoButtonRemove()
    {
        // This will allow us to attached this function to the OnClick event in the Unity inspector to tell it to set active or not

        Cursor.visible = true;

        remove.SetActive(false);
    }
}
