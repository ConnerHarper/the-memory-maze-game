﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI; 


public class MainMenu : MonoBehaviour
{
    // Declare global and local variables - local variables are editable in Unity (can add GameObjects to them etc. or called to another script through GetComponent.)
    // Global variables are only accessable by this script
    public GameObject decision;
    public GameObject memory;
    GameObject question;

    void Update()
    {

        // We will set the cursor to true, as we want the cursor on screen the whole time the player is in the main menu
        Cursor.visible = true;

        // Find the LeaveMazeQuestion GameObject so that we can change how it functions in this script
        question = GameObject.Find("LeaveMazeQuestion");

        // We will ensure the cursor is on screenw hen the question UI element is on screen
        if (question == true)
        {
            Cursor.visible = true;
        }
    }

    public void PlayGame()
    {
        // When this function is called by the OnClick event in the Unity inspector, it will take the player to the next scene in the build (which is the Game scene) 
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void QuitGame()
    {
        // When this function is called by the OnClick event in the Unity inspector, it will quit the game
        // We will use a Debug.Log as we can't check properly that the game actually quits till the game has been built - so for now we will use a Debug.Log to ensure the function works
        Debug.Log("Quit");
        Application.Quit();
    }

    public void MemoryListScreen()
    {
        // This will be run in the Game scene, so if the player clicks "Yes" to the button we will attach this to with the OnClick in the Unity inspector
        // it will take them to the next scene in the build (which is the Memory List scene)
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

    }

    public void UIRemove()
    {
        // If the player has clicked the "Ok" button on the instructions screen, we will set time back to 1 and active again
        Time.timeScale = 1f;
        // We will also disable the decision UI element
        decision.SetActive(false);
    }

    public void DecisionShow()
    {
        // We will set timeScale to 0 so that it freezes the game whilst the decision UI element is being asked/shown to the player
        Time.timeScale = 0f;
        // We will also enable the decision UI element
        decision.SetActive(true);

    }



 

}
