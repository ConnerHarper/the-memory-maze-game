﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOver : MonoBehaviour 
{

    public void PlayAgain()
    {
        // We will load the Game scene if the player clicks "Play Again" on the WinScene or the GameOver scene 
        SceneManager.LoadScene("Game");
    }

    public void Return()
    {
        // We will load the Main Menu scene if the player clicks "Return to Menu" on the WinScene or the GameOver scene 
        SceneManager.LoadScene("MainMenu");
    }


  
}
