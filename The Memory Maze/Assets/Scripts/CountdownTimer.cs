﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class CountdownTimer : MonoBehaviour
{
    // Declare global and local variables - local variables are editable in Unity (can add GameObjects to them etc. or called to another script through GetComponent.)
    // Global variables are only accesable by this script
    public float currentTime = 0f;
    public float startingTime = 20f;

    public Text countdownText;
    public bool timesUp = false;

    GameObject obj;
    PlayerRaycasting time;


    void Start()
    {
        // At the start of the game we want to set the currentTime at present to the StartingTime, which will be 20 seconds
        currentTime = startingTime;

        // We want to find the main camera so that we can enable, disable & re-enable the camera at certain points in the game, as we don't want the player to be able to look about when a UI element is showing (e.g. quiz question)
        obj = GameObject.Find("Main Camera");
        time = obj.GetComponent<PlayerRaycasting>();

    }


    public void Timer()
    {
        
        // We want the currentTime at every frame to count down by 1 second and send the currentTime to a string
        currentTime -= 1 * Time.deltaTime;
        countdownText.text = currentTime.ToString("0");

        Debug.Log("Timer");

        // Reset the timer and run the ResetTimer() function IF it reaches 0 seconds
        if (currentTime <= 0)
        {
            currentTime = 0;

            timesUp = true;

            ResetTimer();
        }


    }

    // This function will be ran every time we want to reset the timer in the scene, this will be when the timer hits 0 seconds or the player has selected one of quiz answers
    public void ResetTimer()
    {
        
        currentTime = startingTime;
        Debug.Log("Reset is called");
        time.stateToggle = false;

        // use QuestionTrigger function from the ButtonTrigger to reset timer when the player has selected one of the quiz options
        time.lastButton.QuestionTrigger();

        timesUp = false;
    }
}
