﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitManager : MonoBehaviour
{
    // Declare global and local variables - local variables are editable in Unity (can add GameObjects to them etc. or called to another script through GetComponent.)
    // Global variables are only accesable by this script
    public List<string> currentAnswers;
    public List<string> actualAnswers;
    GameObject obj;
    GameObject obj2;
    public GameObject textDisplay; 

    public int total;

    // Start is called before the first frame update
    void Start()
    {
        obj2 = GameObject.Find("Cash Total");

        // We will set actualAnswers as GameManager.Answers, so that later on we can split them into 2 seperate array's
        actualAnswers = GameManager.Answers;
        currentAnswers = GameManager.Answers;

        // We will find the InputFIeld from the 
        obj = GameObject.Find("InputField");

        total = currentAnswers.Count;
    }

    // Update is called once per frame
    void Update()
    {
        // We will set up a debug.Log to ensure that the total does reach 0 when all answers from the memory list are inputed and we will proceed to the WinScene if total is 0
        if (total == 0)
        {
            Debug.Log("Hooray u won");
            SceneManager.LoadScene("WinScene");
            
        }

    }

    public void CheckAnswer()
    {
        // We need to get the text component from the input field so that we can see if it's a match to the actualAnswers
        string guess = obj.GetComponent<InputField>().text;

        
        int count = currentAnswers.Count;
        Debug.Log(count);

        // We will use a for loop so that we are able to compare 2 arrays together to see if there is a match between the currentAnswers and the guess that the player gives
        for (int i = 0; i < count; i++)
        {      

            Debug.Log(currentAnswers[i] + guess);


            if (currentAnswers[i] == guess)
            {
                // We will now remove the answer that the player managed to find as a match, and leave the remaining answers to find in the memory list 
                currentAnswers.Remove(currentAnswers[i]);
                total -= 1;
                textDisplay.GetComponent<Text>().text = "Well Done " + guess + " Is A Match!";
                break;
            }
            else
            {
                textDisplay.GetComponent<Text>().text = "NO MATCH";
            }

            
        }


    }

    // This function will be ran if the player clicks the give up button at the bottom of the Mememory List scene
    public void GiveUp()
    {
        SceneManager.LoadScene("GameOver");

        // We will also reset cash incase the player wants to play the game again
        int cash = 0;

    }


}
