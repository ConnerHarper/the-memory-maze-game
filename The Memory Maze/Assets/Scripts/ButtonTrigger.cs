﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonTrigger : MonoBehaviour
{
    // Declare global and local variables - local variables are editable in Unity (can add GameObjects to them etc. or called to another script through GetComponent.)
    // Global variables are only accesable by this script
    public GameObject questionUI;
    // Have a boolean on toggle and pressed variables so we can switch between two scenarios (e.g. button press on true will show UI) 
    bool toggle = false;
    bool pressed = false;

    public AudioClip otherClip;
    AudioSource audioSource;


    // Start is called before the first frame update
    void Start()
    {
        pressed = false;

        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Question trigger function will allow the UI question element to show when a button is pressed in one of the maze rooms, and will loop as long as the game is still playing
    public void QuestionTrigger()
    {
            // if toggle is false then we want to enable the UI (movement & camera will be disabled, code is in other scripts though) 
            if (toggle == false)
            {
                questionUI.SetActive(true);
                toggle = true;
            }
            // if toggle is true then we want to disable the UI
            else
            {
                pressed = true;
                questionUI.SetActive(false);
                toggle = false;
            }


        
    }

    // We will return if the button has been pressed to check that it works effectively
    public bool BeenPressed()
    {
        return pressed;

    }

}
