﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CashAmount : MonoBehaviour
{
    // Declare global and local variables - local variables are editable in Unity (can add GameObjects to them etc. or called to another script through GetComponent.)
    // Global variables are only accesable by this script
    private int cash;
    public Text cashText;

    public GameObject decision;
    GameObject obj;
    MainMenu menu;

    // Status will allow us to tell the game when to start counting down the timer after the cash has been added/updated to the text component
    bool status = false;
    float currTime;

    public GameObject obj2;


    void Start()
    {
        // We need to get the decision component from the MainMenu script so that we can work with the decision UI element in this script
        menu = decision.GetComponent<MainMenu>();

        // We will set cash to 0 at the start of the game 
        cash = 0;
        updateCashUI();
    }

    void Update()
    {
        // We use the boolean status as true so that it updates time with status being enabled 
        if (status == true)
        {        
            TimeUpdate();
        }
    }

    public void addCash(int xAmount)
    {
        // After cash has been updated, we set status as true so we can reset the timer in other scripts
        cash += xAmount;
        updateCashUI();

        status = true;

    }

    void updateCashUI()
    {
        Debug.Log("CASH UPDATE");
        cashText.text = cash.ToString();

        // We will re-enable the cursor after the cash has been added so the player can make there decision to leave the maze or not
        Cursor.visible = true;
 
        // We will reset the timer back to 0 after cash has been added so that it dosen't keep counting down
        currTime = 0;
    }

    void TimeUpdate()
    {
        currTime += 1 * Time.deltaTime;

        // We need an IF statement so that the memory list UI element will dissapear after 10 seconds
        if (currTime > 10)
        {
            status = false;

            // We will run the DecisionShow function that we called from the MainMenu script so that it shows the decision UI element after the memory list has been shown to the player
            menu.DecisionShow();

            obj2.SetActive(!(obj2.activeSelf));


        }

    }

}
