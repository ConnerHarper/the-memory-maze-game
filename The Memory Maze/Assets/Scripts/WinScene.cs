﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WinScene : MonoBehaviour
{

    // We will run this function if the PlayAgain function is called - this will be called if the player clicks on the "Play Again" button the WinScene
    // This will be called with the OnClick event on the Unity inspector 
    public void PlayAgain()
    {
        SceneManager.LoadScene("Game");
    }

    // We will run this function if the Return to Main Menu function is called - this will be called if the player clicks on the "Return To Main Menu" button in the WinScene
    public void Return()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
