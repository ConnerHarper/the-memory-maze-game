﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{

    public float mouseSensitivity = 100f;

    public Transform playerBody;

    float xRotation = 0f;

    public bool stateToggle = false;
    GameObject obj;

    // Start is called before the first frame update
    void Start()
    {
        // Find the Main Camera GameObject so that we can change how it functions in this script
        obj = GameObject.Find("Main Camera");

        Cursor.visible = false;

        playerBody = transform.parent; 
    }

    // Update is called once per frame
    void Update()
    {
        stateToggle = obj.GetComponent<PlayerRaycasting>().stateToggle;

        // These if statements will allow us to toggle between two states (answering a question/moving around the world pressing buttons).
        if (stateToggle == false)
        {
            // We will call the MouseControl function when the StateToggle is disabled
            MouseControl();
        }
        else
        {
            // We must also set cursor back to true when the player stops moving the mouse
            Cursor.visible = true;
        }

    }

    void MouseControl()
    {
        stateToggle = false;

        // We will use a formula to allow the player to move the mouse to look around the world freely
        // This will allow us to move the mouse to look around when the player clicks the screen, which will also disable the mouse cursor
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);

        playerBody.Rotate(Vector3.up * mouseX);

        // We will also disable the cursor when the player is moving the mouse around with the mouse
        Cursor.visible = false;

        
    }
}
