﻿
[System.Serializable]
public class QuestionsAndAnswers
{
    // A temporary script to hold the variables of the questions and answers for the quiz questions (may or may not be deleted at the end of project)
    public string Question;
    public string[] Answers;
    public int CorrectAnswer; 
}
