﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class AnswerScript : MonoBehaviour
{
    // Declare global and local variables - local variables are editable in Unity (can add GameObjects to them etc. or called to another script through GetComponent.)
    // Global variables are only accesable by this script
    public bool isCorrect = false;
    GameObject obj;
    public bool stateToggle = false;
    CountdownTimer time;
    GameObject obj2;
    ButtonTrigger press;

    public GameObject obj3;

    // These variables will be used for the input box and text display feedback on the memory list scene
    public string theInput;
    public GameObject inputField;
    public GameObject textDisplay;

    void Start()
    {
        //We need to find the timer so that we can remove the quiz UI element when the timer reaches 0 or a button on the quiz UI is clicked
        obj = GameObject.Find("Timer");
        time = obj.GetComponent<CountdownTimer>();
  
    }


    //The answer void will run when a button is clicked on the quiz UI element
    public void Answer()
    {
        // We must reset the timer every time a button is clicked
        time.ResetTimer();

        // IF statement will be run every the correct answer is selected on the quiz UI out of the 3 UI elements 
        if (isCorrect == true)

        {


            string correct;

            correct = gameObject.transform.GetChild(1).GetComponent<Text>().text;

            // We will remove the GameObject on the hierarchy so that the player wont need to click it again
            correct = correct.Remove(0, 3);

            // A debug.Log should be put here so that we can check if the correct answer gets removed effectively
            Debug.Log(correct);

            // This will then add it to the memory list which will be used in the memory list scene 
            GameManager.Answers.Add(correct);


        }
        // If isCorrect is false, then we will run this else statement
        else
        {
            // We must create strings - this will be so that we can add all 3 answers to the memory list if the player gets the question incorrect
            string incorrect1, incorrect2, incorrect3;

            // Answer 1 - the shadows on the game that has been downloaded from the unity store, shows a temporary GameObject in the hierarchy called "Inkblot"
            // Therefore we need to remove that when the else function is run for each incorrect answer
            gameObject.transform.parent.transform.parent.transform.GetChild(1).transform.GetChild(0).transform.GetChild(0).SetSiblingIndex(1);

            incorrect1 = gameObject.transform.parent.transform.parent.transform.GetChild(1).transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text;

            // We will remove the GameObject from the hierarchy
            incorrect1 = incorrect1.Remove(0, 3);

            // We will finally add the 1st of the incorrect answers into the memory list 
            GameManager.Answers.Add(incorrect1); 

            // Answer 2
            // We will do the same for Answer 2, but instead we will add the 2nd option to the memory list if the player gets the question wrong
            gameObject.transform.parent.transform.parent.transform.GetChild(2).transform.GetChild(0).transform.GetChild(0).SetSiblingIndex(1);

            incorrect2 = gameObject.transform.parent.transform.parent.transform.GetChild(2).transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text;

            incorrect2 = incorrect2.Remove(0, 3);

            GameManager.Answers.Add(incorrect2);

            // Answer 3
            // We will do the same for Answer 3, but instead we will add the 3rd option to the memory list if the player gets the question wrong
            gameObject.transform.parent.transform.parent.transform.GetChild(3).transform.GetChild(0).transform.GetChild(0).SetSiblingIndex(1);

            incorrect3 = gameObject.transform.parent.transform.parent.transform.GetChild(3).transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text;

            incorrect3 = incorrect3.Remove(0, 3);

            GameManager.Answers.Add(incorrect3);

            // When this ELSE statement is run, all 3 of the possible options should be added to the player's memory list, which we can use and show on other scripts (therefore why it's a public void) 


        }


        if (stateToggle == false)
        {
            obj3.SetActive(!(obj3.activeSelf));
            stateToggle = true;
        }
        // We can use this count integar variable when we need to give the player results at the end e.g. how much words/phrases they rememberd etc. 
        int count = GameManager.Answers.Count;

        obj3.transform.GetChild(2).GetComponent<Text>().text = "";

        // Use a for loop so that we can access the answers that are on the memory list and loop it
        for (int i = 0; i < count; i++)
        {

            obj3.transform.GetChild(2).GetComponent<Text>().text = obj3.transform.GetChild(2).GetComponent<Text>().text + "\n" + GameManager.Answers[i];
        }

        isCorrect = false; 
       
    }

    public void StoreInput()
    {

        theInput = inputField.GetComponent<Text>().text;

    }

   
}
