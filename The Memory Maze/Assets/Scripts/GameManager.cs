﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script will be short and will be used to hold the answer list for the Memory List script as an array
// We can then work with the array for matching the player's guesses to the actualAnswers in the ExitManager script
// We won't make the class a void, but instead a static class so that we are able to use this in other scenes
public static class GameManager
{
    public static List<string> Answers = new List<string>();

}


